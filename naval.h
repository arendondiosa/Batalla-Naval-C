#include <cstdlib>
#include <stdlib.h>
#include <time.h>

using namespace std;

int barcos_dos_cpu = 0;
int barcos_tres_cpu = 0;
int barcos_cuatro_cpu = 0;
int barcos_cinco_cpu = 0;
int cpu = 0, jugador = 0;


void imprimir(char tablero1[10][10]) {
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      cout << tablero1[i][j];
    }
    cout << endl;
  }
}

void imprimir_ambos(char tablero1[10][10], char tablero2[10][10]) {
  cout << "JUGADOR     CPU" << endl;
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      cout << tablero1[i][j];
    }
    cout << "  ";
    for (int j = 0; j < 10; j++) {
      cout << tablero2[i][j];
    }
    cout << endl;
  }
}

void llenar(char tablero[10][10]) {
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      tablero[i][j] = '*';
    }
  }
}

void vacio(char tablero[10][10]) {
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      tablero[i][j] = ' ';
    }
  }
}

bool numValido(int x) {
  if (x >= 0 && x <= 9) return true;
  else return false;
}

bool dirValida(char c) {
  if (c == 'u' || c == 'd' || c == 'l' || c == 'r') return true;
  else return false;
}

//--------------------Crear Barcos
void barcos_dos(char tablero[10][10]) {
  int aleatorio_h = rand() % 10;
  int aleatorio_v = rand() % 10;

  //cout << "Aleatorios " << aleatorio_h << "  " << aleatorio_v << endl;
  if (tablero[aleatorio_h][aleatorio_v] == ' ')
    if (numValido(aleatorio_h + 1) && tablero[aleatorio_h + 1][aleatorio_v] == ' ') {
      tablero[aleatorio_h + 1][aleatorio_v] = '|';
      tablero[aleatorio_h][aleatorio_v] = '|';
      barcos_dos_cpu++;
    }
    else if (numValido(aleatorio_h - 1) && tablero[aleatorio_h - 1][aleatorio_v] == ' ') {
      tablero[aleatorio_h - 1][aleatorio_v] = '|';
      tablero[aleatorio_h][aleatorio_v] = '|';
      barcos_dos_cpu++;
    }
    else if (numValido(aleatorio_v + 1) && tablero[aleatorio_h][aleatorio_v + 1] == ' ') {
      tablero[aleatorio_h][aleatorio_v + 1] = '-';
      tablero[aleatorio_h][aleatorio_v] = '-';
      barcos_dos_cpu++;
    }
    else if (numValido(aleatorio_v - 1) && tablero[aleatorio_h][aleatorio_v - 1] == ' ') {
      tablero[aleatorio_h][aleatorio_v - 1] = '-';
      tablero[aleatorio_h][aleatorio_v] = '-';
      barcos_dos_cpu++;
    }
}

void barcos_tres(char tablero[10][10]) {
  int aleatorio_h = rand() % 10;
  int aleatorio_v = rand() % 10;
  //cout << "Aleatorios " << aleatorio_h << "  " << aleatorio_v << endl;
  if (tablero[aleatorio_h][aleatorio_v] == ' ') {
    if ((numValido(aleatorio_h + 1) && tablero[aleatorio_h + 1][aleatorio_v] == ' ') &&
      (numValido(aleatorio_h - 1) && tablero[aleatorio_h - 1][aleatorio_v] == ' ')) {
      tablero[aleatorio_h + 1][aleatorio_v] = '|';
      tablero[aleatorio_h - 1][aleatorio_v] = '|';
      tablero[aleatorio_h][aleatorio_v] = '|';
      barcos_tres_cpu++;
    }
    else if ((numValido(aleatorio_v + 1) && tablero[aleatorio_h][aleatorio_v + 1] == ' ') &&
      (numValido(aleatorio_v - 1) && tablero[aleatorio_h][aleatorio_v - 1] == ' ')) {
      tablero[aleatorio_h][aleatorio_v + 1] = '-';
      tablero[aleatorio_h][aleatorio_v - 1] = '-';
      tablero[aleatorio_h][aleatorio_v] = '-';
      barcos_tres_cpu++;
    }
  }
}

void barcos_cuatro(char tablero[10][10]) {
  int aleatorio_h = rand() % 10;
  int aleatorio_v = rand() % 10;

  if (tablero[aleatorio_h][aleatorio_v] == ' ') {
    if ((numValido(aleatorio_h + 1) && tablero[aleatorio_h + 1][aleatorio_v] == ' ') &&
      (numValido(aleatorio_h + 2) && tablero[aleatorio_h + 2][aleatorio_v] == ' ') &&
      (numValido(aleatorio_h - 1) && tablero[aleatorio_h - 1][aleatorio_v] == ' ')) {
      tablero[aleatorio_h + 1][aleatorio_v] = '|';
      tablero[aleatorio_h - 1][aleatorio_v] = '|';
      tablero[aleatorio_h + 2][aleatorio_v] = '|';
      tablero[aleatorio_h][aleatorio_v] = '|';
      barcos_cuatro_cpu++;
    }
    else if ((numValido(aleatorio_h + 1) && tablero[aleatorio_h + 1][aleatorio_v] == ' ') &&
      (numValido(aleatorio_h - 2) && tablero[aleatorio_h - 2][aleatorio_v] == ' ') &&
      (numValido(aleatorio_h - 1) && tablero[aleatorio_h - 1][aleatorio_v] == ' ')) {
      tablero[aleatorio_h + 1][aleatorio_v] = '|';
      tablero[aleatorio_h - 1][aleatorio_v] = '|';
      tablero[aleatorio_h - 2][aleatorio_v] = '|';
      tablero[aleatorio_h][aleatorio_v] = '|';
      barcos_cuatro_cpu++;
    }
    else if ((numValido(aleatorio_v + 1) && tablero[aleatorio_h][aleatorio_v + 1] == ' ') &&
      (numValido(aleatorio_v + 2) && tablero[aleatorio_h][aleatorio_v + 2] == ' ') &&
      (numValido(aleatorio_v - 1) && tablero[aleatorio_h][aleatorio_v - 1] == ' ')) {
      tablero[aleatorio_h][aleatorio_v + 1] = '-';
      tablero[aleatorio_h][aleatorio_v - 1] = '-';
      tablero[aleatorio_h][aleatorio_v + 2] = '-';
      tablero[aleatorio_h][aleatorio_v] = '-';
      barcos_cuatro_cpu++;
    }
    else if ((numValido(aleatorio_v + 1) && tablero[aleatorio_h][aleatorio_v + 1] == ' ') &&
      (numValido(aleatorio_v - 2) && tablero[aleatorio_h][aleatorio_v - 2] == ' ') &&
      (numValido(aleatorio_v - 1) && tablero[aleatorio_h][aleatorio_v - 1] == ' ')) {
      tablero[aleatorio_h][aleatorio_v + 1] = '-';
      tablero[aleatorio_h][aleatorio_v - 1] = '-';
      tablero[aleatorio_h][aleatorio_v - 2] = '-';
      tablero[aleatorio_h][aleatorio_v] = '-';
      barcos_cuatro_cpu++;
    }
  }
}

void barcos_cinco(char tablero[10][10]) {
  int aleatorio_h = rand() % 10;
  int aleatorio_v = rand() % 10;

  if (tablero[aleatorio_h][aleatorio_v] == ' ') {
    if ((numValido(aleatorio_h + 1) && tablero[aleatorio_h + 1][aleatorio_v] == ' ') &&
      (numValido(aleatorio_h + 2) && tablero[aleatorio_h + 2][aleatorio_v] == ' ') &&
      (numValido(aleatorio_h - 2) && tablero[aleatorio_h - 2][aleatorio_v] == ' ') &&
      (numValido(aleatorio_h - 1) && tablero[aleatorio_h - 1][aleatorio_v] == ' ')) {
      tablero[aleatorio_h + 1][aleatorio_v] = '|';
      tablero[aleatorio_h - 1][aleatorio_v] = '|';
      tablero[aleatorio_h + 2][aleatorio_v] = '|';
      tablero[aleatorio_h - 2][aleatorio_v] = '|';
      tablero[aleatorio_h][aleatorio_v] = '|';
      barcos_cinco_cpu++;
    }
    else if ((numValido(aleatorio_v + 1) && tablero[aleatorio_h][aleatorio_v + 1] == ' ') &&
      (numValido(aleatorio_v + 2) && tablero[aleatorio_h][aleatorio_v + 2] == ' ') &&
      (numValido(aleatorio_v - 2) && tablero[aleatorio_h][aleatorio_v - 2] == ' ') &&
      (numValido(aleatorio_v - 1) && tablero[aleatorio_h][aleatorio_v - 1] == ' ')) {
      tablero[aleatorio_h][aleatorio_v + 1] = '-';
      tablero[aleatorio_h][aleatorio_v - 1] = '-';
      tablero[aleatorio_h][aleatorio_v + 2] = '-';
      tablero[aleatorio_h][aleatorio_v - 2] = '-';
      tablero[aleatorio_h][aleatorio_v] = '-';
      barcos_cinco_cpu++;
    }
  }
}

void barcos_cpu(char tablero[10][10]) {
//1 barco de tam 2
  while (barcos_dos_cpu < 1) {
    barcos_dos(tablero);
  }
//2 barco de tam 3
  while (barcos_tres_cpu < 2) {
    barcos_tres(tablero);
  }
//1 barco de tam 4
  while (barcos_cuatro_cpu < 1) {
    barcos_cuatro(tablero);
  }
//1 barco de tam 5
  while (barcos_cinco_cpu < 1) {
    barcos_cinco(tablero);
  }
}

// --------------------------------------------------------------------------
//BARCOS JUGADOR : Posiciones son del 1 al 10

bool valid_dos (char tablero[10][10], char dir, int x, int y) {
  if (dir == 'u') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 2][y - 1] == ' ' && numValido(x - 2)) {
      tablero[x - 1][y - 1] = '|';
      tablero[x - 2][y - 1] = '|';
      return true;
    }
    else return false;
  }
  else if (dir == 'd') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x][y - 1] == ' ' && numValido(x)) {
      tablero[x - 1][y - 1] = '|';
      tablero[x][y - 1] = '|';
      return true;
    }
    else return false;
  }
  else if (dir == 'l') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 1][y - 2] == ' ' && numValido(y - 2)) {
      tablero[x - 1][y - 1] = '-';
      tablero[x - 1][y - 2] = '-';
      return true;
    }
    else return false;
  }
  else if (dir == 'r') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 1][y] == ' ' && numValido(y)) {
      tablero[x - 1][y - 1] = '-';
      tablero[x - 1][y] = '-';
      return true;
    }
    else return false;
  }
}

void barco_dos_jugador(char tablero[10][10]) {
  int x = -1, y = -1;
  char dir = ' ';

  while (!valid_dos(tablero, dir, x, y)) {
    system("cls");
    system("clear");
    cout << "Barco de tamaño 2" << endl;
    imprimir(tablero);
    cout << "INGRESE POSICION Y ORIENTACION VALIDA" << endl;
    x = -1; y = -1; dir = ' ';
    while (!numValido(x - 1)) {
      cout << "Ingrese posición inicial en x" << endl;
      cin >> x;
    }
    while (!numValido(y - 1)) {
      cout << "Ingrese posición inicial en y" << endl;
      cin >> y;
    }
    while (!dirValida(dir)) {
      cout << "Ingrese orientacion del barco" << endl;
      cin >> dir;
    }
  }
  system("cls");
  system("clear");
  cout << "Se creo el barco de tamaño 2" << endl;
}

//tamaño 3 jugador
bool valid_tres (char tablero[10][10], char dir, int x, int y) {
  if (dir == 'u') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 2][y - 1] == ' ' && tablero[x - 3][y - 1] == ' ' &&
      numValido(x - 2) && numValido(x - 3)) {
      tablero[x - 1][y - 1] = '|';
      tablero[x - 2][y - 1] = '|';
      tablero[x - 3][y - 1] = '|';
      return true;
    }
    else return false;
  }
  else if (dir == 'd') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x][y - 1] == ' ' && tablero[x + 1][y - 1] == ' ' &&
      numValido(x) && numValido(x + 1)) {
      tablero[x - 1][y - 1] = '|';
      tablero[x][y - 1] = '|';
      tablero[x + 1][y - 1] = '|';
      return true;
    }
    else return false;
  }
  else if (dir == 'l') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 1][y - 2] == ' ' && tablero[x - 1][y - 3] == ' ' &&
      numValido(y - 2) && numValido(y - 3)) {
      tablero[x - 1][y - 1] = '-';
      tablero[x - 1][y - 2] = '-';
      tablero[x - 1][y - 3] = '-';
      return true;
    }
    else return false;
  }
  else if (dir == 'r') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 1][y] == ' ' && tablero[x - 1][y + 1] == ' ' &&
      numValido(y) && numValido(y + 1)) {
      tablero[x - 1][y - 1] = '-';
      tablero[x - 1][y] = '-';
      tablero[x - 1][y + 1] = '-';
      return true;
    }
    else return false;
  }
}

void barco_tres_jugador(char tablero[10][10]) {
  int x = -1, y = -1;
  char dir = ' ';

  while (!valid_tres(tablero, dir, x, y)) {
    system("cls");
    system("clear");
    cout << "Barco de tamaño 3" << endl;
    imprimir(tablero);
    cout << "INGRESE POSICION Y ORIENTACION VALIDA" << endl;
    x = -1; y = -1; dir = ' ';
    while (!numValido(x - 1)) {
      cout << "Ingrese posición inicial en x" << endl;
      cin >> x;
    }
    while (!numValido(y - 1)) {
      cout << "Ingrese posición inicial en y" << endl;
      cin >> y;
    }
    while (!dirValida(dir)) {
      cout << "Ingrese orientacion del barco" << endl;
      cin >> dir;
    }
  }
  system("cls");
  system("clear");
  cout << "Se creo el barco de tamaño 3" << endl;

}

//tamaño 4 jugador
bool valid_cuatro (char tablero[10][10], char dir, int x, int y) {
  if (dir == 'u') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 2][y - 1] == ' ' && tablero[x - 3][y - 1] == ' ' && tablero[x - 4][y - 1] == ' ' &&
      numValido(x - 2) && numValido(x - 3) && numValido(x - 4)) {
      tablero[x - 1][y - 1] = '|';
      tablero[x - 2][y - 1] = '|';
      tablero[x - 3][y - 1] = '|';
      tablero[x - 4][y - 1] = '|';
      return true;
    }
    else return false;
  }
  else if (dir == 'd') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x][y - 1] == ' ' && tablero[x + 1][y - 1] == ' ' && tablero[x + 2][y - 1] == ' ' &&
      numValido(x) && numValido(x + 1) && numValido(x + 2)) {
      tablero[x - 1][y - 1] = '|';
      tablero[x][y - 1] = '|';
      tablero[x + 1][y - 1] = '|';
      tablero[x + 2][y - 1] = '|';
      return true;
    }
    else return false;
  }
  else if (dir == 'l') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 1][y - 2] == ' ' && tablero[x - 1][y - 3] == ' ' && tablero[x - 1][y - 4] == ' ' &&
      numValido(y - 2) && numValido(y - 3) && numValido(y - 4)) {
      tablero[x - 1][y - 1] = '-';
      tablero[x - 1][y - 2] = '-';
      tablero[x - 1][y - 3] = '-';
      tablero[x - 1][y - 4] = '-';
      return true;
    }
    else return false;
  }
  else if (dir == 'r') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 1][y] == ' ' && tablero[x - 1][y + 1] == ' ' && tablero[x - 1][y + 2] == ' ' &&
      numValido(y) && numValido(y + 1) && numValido(y + 2)) {
      tablero[x - 1][y - 1] = '-';
      tablero[x - 1][y] = '-';
      tablero[x - 1][y + 1] = '-';
      tablero[x - 1][y + 2] = '-';
      return true;
    }
    else return false;
  }
}

void barco_cuatro_jugador(char tablero[10][10]) {
  int x = -1, y = -1;
  char dir = ' ';

  while (!valid_cuatro(tablero, dir, x, y)) {
    system("cls");
    system("clear");
    cout << "Barco de tamaño 4" << endl;
    imprimir(tablero);
    cout << "INGRESE POSICION Y ORIENTACION VALIDA" << endl;
    x = -1; y = -1; dir = ' ';
    while (!numValido(x - 1)) {
      cout << "Ingrese posición inicial en x" << endl;
      cin >> x;
    }
    while (!numValido(y - 1)) {
      cout << "Ingrese posición inicial en y" << endl;
      cin >> y;
    }
    while (!dirValida(dir)) {
      cout << "Ingrese orientacion del barco" << endl;
      cin >> dir;
    }
  }
  system("cls");
  system("clear");
  cout << "Se creo el barco de tamaño 4" << endl;
}

//tamaño 5 jugador
bool valid_cinco (char tablero[10][10], char dir, int x, int y) {
  if (dir == 'u') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 2][y - 1] == ' ' && tablero[x - 3][y - 1] == ' ' && tablero[x - 4][y - 1] == ' ' &&  tablero[x - 5][y - 1] == ' ' &&
      numValido(x - 2) && numValido(x - 3) && numValido(x - 4) && numValido(x - 5)) {
      tablero[x - 1][y - 1] = '|';
      tablero[x - 2][y - 1] = '|';
      tablero[x - 3][y - 1] = '|';
      tablero[x - 4][y - 1] = '|';
      tablero[x - 5][y - 1] = '|';
      return true;
    }
    else return false;
  }
  else if (dir == 'd') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x][y - 1] == ' ' && tablero[x + 1][y - 1] == ' ' && tablero[x + 2][y - 1] == ' ' &&  tablero[x + 3][y - 1] == ' ' &&
      numValido(x) && numValido(x + 1) && numValido(x + 2) && numValido(x + 3)) {
      tablero[x - 1][y - 1] = '|';
      tablero[x][y - 1] = '|';
      tablero[x + 1][y - 1] = '|';
      tablero[x + 2][y - 1] = '|';
      tablero[x + 3][y - 1] = '|';
      return true;
    }
    else return false;
  }
  else if (dir == 'l') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 1][y - 2] == ' ' && tablero[x - 1][y - 3] == ' ' && tablero[x - 1][y - 4] == ' ' &&  tablero[x - 1][y - 5] == ' ' &&
      numValido(y - 2) && numValido(y - 3) && numValido(y - 4) && numValido(y - 5)) {
      tablero[x - 1][y - 1] = '-';
      tablero[x - 1][y - 2] = '-';
      tablero[x - 1][y - 3] = '-';
      tablero[x - 1][y - 4] = '-';
      tablero[x - 1][y - 5] = '-';
      return true;
    }
    else return false;
  }
  else if (dir == 'r') {
    if (tablero[x - 1][y - 1] == ' ' && tablero[x - 1][y] == ' ' && tablero[x - 1][y + 1] == ' ' && tablero[x - 1][y + 2] == ' ' &&  tablero[x - 1][y + 3] == ' ' &&
      numValido(y) && numValido(y + 1) && numValido(y + 2)) {
      tablero[x - 1][y - 1] = '-';
      tablero[x - 1][y] = '-';
      tablero[x - 1][y + 1] = '-';
      tablero[x - 1][y + 2] = '-';
      tablero[x - 1][y + 3] = '-';
      return true;
    }
    else return false;
  }
}

void barco_cinco_jugador(char tablero[10][10]) {
  int x = -1, y = -1;
  char dir = ' ';

  while (!valid_cinco(tablero, dir, x, y)) {
    system("cls");
    system("clear");
    cout << "Barco de tamaño 4" << endl;
    imprimir(tablero);
    cout << "INGRESE POSICION Y ORIENTACION VALIDA" << endl;
    x = -1; y = -1; dir = ' ';
    while (!numValido(x - 1)) {
      cout << "Ingrese posición inicial en x" << endl;
      cin >> x;
    }
    while (!numValido(y - 1)) {
      cout << "Ingrese posición inicial en y" << endl;
      cin >> y;
    }
    while (!dirValida(dir)) {
      cout << "Ingrese orientacion del barco" << endl;
      cin >> dir;
    }
  }
  system("cls");
  system("clear");
  cout << "Se creo el barco de tamaño 4" << endl;
}

//Crea todos los barcos del jugador
void barcos_jugador(char tablero[10][10]) {
  barco_dos_jugador(tablero);
  barco_tres_jugador(tablero);
  barco_tres_jugador(tablero);
  barco_cuatro_jugador(tablero);
  barco_cinco_jugador(tablero);
}
//-----------------------

// Jugar cpu
void jugar_cpu(char tablero1[10][10], char tablero2[10][10]) {
  int aleatorio_h = rand() % 10;
  int aleatorio_v = rand() % 10;

  while (!numValido(aleatorio_v) && !numValido(aleatorio_h) && tablero2[aleatorio_h][aleatorio_v] == 'o' && tablero2[aleatorio_h][aleatorio_v] == '.') {
    int aleatorio_h = rand() % 10;
    int aleatorio_v = rand() % 10;
  }

  if (tablero1[aleatorio_h][aleatorio_v] == '-' || tablero1[aleatorio_h][aleatorio_v] == '|') {
    tablero1[aleatorio_h][aleatorio_v] = 'o';
    tablero2[aleatorio_h][aleatorio_v] = 'o';
    cpu++;
  }
  else if (tablero2[aleatorio_h][aleatorio_v] != 'o'){
    tablero1[aleatorio_h][aleatorio_v] = '.';
    tablero2[aleatorio_h][aleatorio_v] = '.';
  }
}

// Jugar jugador
void jugar_jugador(char tablero1[10][10], char tablero2[10][10]) {
  cout << "INGRESE POSICION Y ORIENTACION VALIDA" << endl;
  int x = -1; int y = -1;
  while (!numValido(x - 1)) {
    cout << "Ingrese posición inicial en x" << endl;
    cin >> x;
  }
  while (!numValido(y - 1)) {
    cout << "Ingrese posición inicial en y" << endl;
    cin >> y;
  }

  if (tablero1[x - 1][y - 1] == '-' || tablero1[x - 1][y - 1] == '|') {
    tablero1[x - 1][y - 1] = 'o';
    tablero2[x - 1][y - 1] = 'o';
    jugador++;
  }
  else if (tablero2[x - 1][y - 1] != 'o'){
    tablero1[x - 1][y - 1] = '.';
    tablero2[x - 1][y - 1] = '.';
  }
}
