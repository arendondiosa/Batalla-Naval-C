#include <iostream>
#include "naval.h"

//FUNCION PRINCIPAL

int main() {
  srand(time(NULL)); //Iniciar vector numeros ramdom
  barcos_dos_cpu = 0;
  barcos_tres_cpu = 0;
  barcos_cuatro_cpu = 0;
  barcos_cinco_cpu = 0;
  cpu = 0;
  jugador = 0;
  char salir = ' ';
  bool jugar = true;

  while (true) {
    system("cls");
    system("clear");
    //Tableros
    char cpu1[10][10], cpu2[10][10], jugador1[10][10], jugador2[10][10];

    vacio(cpu1);
    vacio(jugador1);
    llenar(cpu2);
    llenar(jugador2);

  //Llenar tablero cpu con barcos
    barcos_cpu(cpu1);
    barcos_jugador(jugador1);

    while (jugar) {
      system("cls");
      system("clear");
      imprimir_ambos(jugador1, cpu2);
      cout << endl;
      cout << "TURNO CPU" << endl;
      cout << endl;
      jugar_cpu(jugador1, jugador2);
      cout << endl;
      imprimir_ambos(jugador1, cpu2);
      cout << endl;
      cout << "TURNO JUGADOR" << endl;
      cout << endl;
      jugar_jugador(cpu1, cpu2);

      if (cpu == 17 || jugador == 17) {
        jugar = false;
      }
    }

    // Visualizar

    system("cls");
    system("clear");
    imprimir_ambos(jugador1, cpu1);
    cout << endl;

    if (cpu == 17) cout << "GANO CPU :(" << endl;
    else if (jugador == 17) cout << "FELICIDADES :) !!!" << endl;

    cout << endl;
    cout << "DESEA JUGAR DE NUEVO? s/n" << endl;
    cin >> salir;
    if (salir == 'n') break;
  }

  return 0;
}
